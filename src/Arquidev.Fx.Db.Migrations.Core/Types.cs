﻿using Arquidev.Fx.Hosting.StartupHooks;
using System.Reflection;
using DbUp;
using DbUp.Builder;
using DbUp.Engine;
using DbUp.Engine.Output;
using DbUp.ScriptProviders;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Arquidev.Fx.Db.Migrations.Core;

public delegate UpgradeEngineBuilder UpgradeBuilder();
public delegate void EnsureDb();
public delegate void ConfigureEnsureDb(SupportedDatabasesForEnsureDatabase supportedDatabases, string connectionString, IUpgradeLog upgradeLog);
public delegate UpgradeEngineBuilder ConfigureDeployDb(SupportedDatabases supportedDatabases, string connectionString);
public delegate Assembly ConfigureScriptsAssembly();

public interface IProcessTerminator
{
    void Exit(int exitCode);
}

public class Terminator : IProcessTerminator
{
    public void Exit(int exitCode) => Environment.Exit(exitCode);
}

public class DbMigrationsSettings
{
    public bool Enabled { get; set; } = true;
    public bool EnsureDatabase { get; set; } = true;
    ///<summary>
    /// The scripts located under this path in the file system
    /// run in addition to the embedded migration scripts.
    /// </summary>
    public string? AdditionalScriptsPath { get; set; }

    /// <summary>
    /// When set to true runs additional scripts specified by <see cref="AdditionalScriptsPath"/> after the embedded migration scripts.
    /// Default runs additional scripts before the embedded migration scripts.
    /// </summary>
    public bool AdditionalScriptsAfter { get; set; }

    /// <summary>
    /// If set to true, when migrations get successfully applied, the process exits with ExitCode = 0
    /// </summary>
    public bool ExitOnSuccess { get; set; }
}

public class MigrationService(
    UpgradeBuilder configureDbUp,
    IOptions<DbMigrationsSettings> dbMigrations,
    IProcessTerminator terminator,
    EnsureDb configureEnsureDb,
    ILogger<MigrationService> logger,
    IHostEnvironment env
    )
{
    private readonly DbMigrationsSettings _dbMigrations = dbMigrations?.Value ?? throw new ArgumentNullException(nameof(dbMigrations));
    private readonly UpgradeBuilder _configureEngine = configureDbUp;
    private readonly IProcessTerminator _terminator = terminator;
    private readonly EnsureDb _ensureDatabase = configureEnsureDb;
    private readonly ILogger<MigrationService> _logger = logger;
    private readonly IHostEnvironment _env = env;

#pragma warning disable IDE0060 // Remove unused parameter
    public Task StartAsync(CancellationToken cancellationToken)
#pragma warning restore IDE0060 // Remove unused parameter
    {
        using var _ = _logger.BeginScope("DB migrations");

        if (!_dbMigrations.Enabled)
        {
            _logger.LogDebug("Disabled via configuration. Skipping");
            return Task.CompletedTask;
        }

        _logger.LogDebug("Starting");
        if (_dbMigrations.EnsureDatabase)
        {
            _logger.LogInformation("Ensuring database");
            _ensureDatabase();
        }
        else
        {
            _logger.LogDebug("Skipped EnsureDatabase");
        }

        var result = _configureEngine().Build().PerformUpgrade();
        _logger.LogInformation("OK");

        if (!result.Successful)
        {
            _logger.LogError(result.Error, "Script: {script}", result.ErrorScript.Name);
            _terminator.Exit(1);
        }

        if (_env.IsDevelopment() || !_dbMigrations.ExitOnSuccess)
        {
            _logger.LogDebug("Now the app will resume execution. ExitOnSuccess: {ExitOnSuccess}, Environment: {Environment}", _dbMigrations.ExitOnSuccess, _env.EnvironmentName);
            return Task.CompletedTask;
        }
        _logger.LogDebug("Terminating. ExitOnSuccess: {ExitOnSuccess}, Environment: {Environment}", _dbMigrations.ExitOnSuccess, _env.EnvironmentName);
        _terminator.Exit(0);
        return Task.CompletedTask;
    }
}

public static class Extensions
{

    public class DbMigrationsCategory { }
    public delegate string GetConnectionString();
    /// <summary>
    /// Configures DbUp migrations. The scripts need to be embedded in the entry assembly or the passed assembly.
    /// </summary>
    /// <param name="builder">Host builder instance</param>
    /// <param name="configureEnsureDb">Configures the db engine for ensuring database</param>
    /// <param name="configureDeployDb">Configures the db engine for scripts deployment</param>
    /// <param name="getConnectionString">Database connection string</param>
    /// <param name="configureScriptsAssembly">Allows passing an assembly the embedded scripts should be read from instead of the entry assembly</param>
    /// <returns>Host builder instance</returns>
    public static IHostBuilder AddDbUpMigrationsCore(this IHostBuilder builder, ConfigureEnsureDb configureEnsureDb,
        ConfigureDeployDb configureDeployDb, Func<HostBuilderContext, ILogger<DbMigrationsCategory>, string> getConnectionString, ConfigureScriptsAssembly? configureScriptsAssembly = null)
    {
        builder.ConfigureServices((ctx, services) =>
        {
            services.Configure<DbMigrationsSettings>(ctx.Configuration.GetSection("dbMigrations"));
            services.AddSingleton(configureEnsureDb);
            services.AddSingleton<GetConnectionString>(f =>
            {
                var lazy = new Lazy<string>(() =>
                    getConnectionString(ctx, f.GetRequiredService<ILogger<DbMigrationsCategory>>()),
                    LazyThreadSafetyMode.ExecutionAndPublication);
                return () => lazy.Value;
            });
            services.AddSingleton<UpgradeBuilder>(f => () =>
            {
                var cfg = f.GetRequiredService<IOptions<DbMigrationsSettings>>()?.Value
                    ?? throw new InvalidOperationException("'dbMigrations' config section is not configured");

                var assembly = configureScriptsAssembly?.Invoke() ?? Assembly.GetEntryAssembly();
                var dbBuilder = configureDeployDb(DeployChanges.To, f.GetRequiredService<GetConnectionString>()())
                    .WithScriptsEmbeddedInAssembly(assembly)
                    .LogToAutodetectedLog();
                if (cfg.AdditionalScriptsPath is { } path)
                {
                    dbBuilder.WithScriptsFromFileSystem(path, new FileSystemScriptOptions { IncludeSubDirectories = true },
                    new SqlScriptOptions
                    {
                        RunGroupOrder = cfg.AdditionalScriptsAfter ?
                        DbUpDefaults.DefaultRunGroupOrder + 1 : DbUpDefaults.DefaultRunGroupOrder - 1
                    });
                }
                return dbBuilder;
            });
            services.AddSingleton<EnsureDb>(f => () =>
                configureEnsureDb(DbUp.EnsureDatabase.For, f.GetRequiredService<GetConnectionString>()(), new AutodetectUpgradeLog()));
            services.AddSingleton<IProcessTerminator, Terminator>();
            services.AddSingleton<MigrationService>();
            services.OnBeforeHostStarted(50, "Database migration", async (s, token) =>
            {
                await s.GetRequiredService<MigrationService>().StartAsync(token);
            });
        });
        return builder;
    }
}
