# fx/db/migrations/core

The core package for DbUp migrations. Please use database-specific packages. E.g. fx/db/migrations/mssql

2024 © [Arquidev](https://arquidev.com)
