﻿using Arquidev.Fx.Db.Migrations.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Arquidev.Fx.Db.Migrations.MsSql;

public static class Extensions
{
    /// <summary>
    /// Adds DbUp migrations. The scripts need to be embedded in the entry assembly. It uses connection string named 'sql' by default.
    /// </summary>
    /// <param name="builder">Host builder instance</param>
    /// <param name="connectionStringName">Connection string name</param>
    /// <param name="configureScriptsAssembly">Can discover scripts from provided assembly rather than the entry assembly</param>
    /// <returns>Host builder instance</returns>
    public static IHostBuilder AddDbUpMigrations(this IHostBuilder builder, string connectionStringName = "sql", ConfigureScriptsAssembly? configureScriptsAssembly = null) =>
        builder.AddDbUpMigrationsCore(
            (db, connStr, log) => db.SqlDatabase(connStr, log),
            (db, connStr) => db.SqlDatabase(connStr),
            (ctx, logger) =>
            {
                using var _ = logger.BeginScope("Db Connection String");
                logger.LogDebug("Reading connection string {connectionStringName}", connectionStringName);
                var c = ctx.Configuration;
                return c.GetConnectionString(connectionStringName)!;

            }, configureScriptsAssembly);
}
