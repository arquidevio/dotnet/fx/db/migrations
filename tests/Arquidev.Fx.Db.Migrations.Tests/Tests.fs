module Arquidev.Fx.Db.Migrations.Tests

open Arquidev.Fx.Db.Migrations.Core
open Expecto
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open System
open System.IO
open Microsoft.Extensions.Logging
open DbUp.SQLite.Helpers
open Microsoft.Data.Sqlite
open System.Reflection

type Marker = unit

[<Tests>]
let tests =
    testList
        "Run migrations"
        [
            testTask "Comparer" {

                use connection = new SqliteConnection("Data Source=:memory:;Cache=Shared;")
                connection.Open()


                let host = HostBuilder()

                host.ConfigureHostConfiguration(fun x ->
                    x.AddInMemoryCollection(
                        [
                            ("Environment", "Development")
                            ("dbMigrations:enabled", "true")
                            ("dbMigrations:ensureDatabase", "true")
                            ("dbMigrations:additionalScriptsPath",
                             $"{Path.GetDirectoryName(typeof<Marker>.Assembly.Location |> string)}/AdditionalScripts")
                        ]
                        |> dict
                    )
                    |> ignore)
                |> ignore

                let ensureDb = fun _ _ _ -> ()

                let deployDb =
                    ConfigureDeployDb(fun db _ -> db.SQLiteDatabase(new SharedConnection(connection)))

                let connString = fun _ _ -> ""
                let assembly = fun () -> Assembly.GetEntryAssembly()

                host.AddDbUpMigrationsCore(ensureDb, deployDb, connString, assembly) |> ignore

                let h = host.Build()

                let upgradeBuilder = h.Services.GetRequiredService<UpgradeBuilder>()
                upgradeBuilder.Invoke().Build().PerformUpgrade() |> ignore
            }
        ]
